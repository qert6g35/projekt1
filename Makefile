TRGDIR=./
OBJ=./obj
FLAGS= -Wall -pedantic -std=c++17 -iquote inc

__start__: ${TRGDIR}/Pro1
	${TRGDIR}/Pro1

${TRGDIR}/Pro1: ${OBJ} ${OBJ}/main.o ${OBJ}/Pakiet.o  ${OBJ}/Operator.o
	g++ -o ${TRGDIR}/Pro1 ${OBJ}/main.o ${OBJ}/Pakiet.o  ${OBJ}/Operator.o

${OBJ}:
	mkdir ${OBJ}

${OBJ}/main.o: src/main.cpp inc/Pakiet.hh inc/Operator.hh
	g++ -c ${FLAGS} -o ${OBJ}/main.o src/main.cpp

${OBJ}/Pakiet.o: src/Pakiet.cpp inc/Pakiet.hh
	g++ -c ${FLAGS} -o ${OBJ}/Pakiet.o src/Pakiet.cpp

${OBJ}/Operator.o: src/Operator.cpp inc/Operator.hh inc/Pakiet.hh
	g++ -c ${FLAGS} -o ${OBJ}/Operator.o src/Operator.cpp
	
clear:
	rm -f ${TRGDIR}/Pro1 ${OBJ}/*

