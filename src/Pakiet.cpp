#include "Pakiet.hh"


PakietDanych::PakietDanych(char NowyZnak, PakietDanych * NastNowy, unsigned int NowyID){
    Znak = NowyZnak;
    Nast = NastNowy;
    Pop = nullptr;
    ID = NowyID;
}

PakietDanych::PakietDanych(char NowyZnak, unsigned int NowyID){
    Znak = NowyZnak;
    Nast = nullptr;
    Pop = nullptr;
    ID = NowyID;  
}

PakietDanych::PakietDanych(){
    Znak = ' ';
    Nast = nullptr;
    Pop = nullptr;
    ID = 0;  
}

void PakietDanych::Wysw(){

std::cout<<Znak<<" "<<ID;

}

char PakietDanych::getZnak(){
    return Znak;
}

void PakietDanych::setZnak(char NowyZnak){
    Znak = NowyZnak;
}

unsigned int PakietDanych::getID(){
    return ID;
}

PakietDanych* PakietDanych::getNast(){
    return Nast;
}

void PakietDanych::setNast(PakietDanych * NowyPakiet){
    Nast = NowyPakiet;
}

PakietDanych* PakietDanych::getPop(){
    return Pop;
}

void PakietDanych::setPop(PakietDanych * NowyPakiet){
    Pop = NowyPakiet;
}
