#include "Operator.hh"

int pow10(int doktorej){
if(doktorej == 1) return 1;
if(doktorej == 0) return 1;
return (10 * pow10(doktorej-1));
}

Operator::Operator(){
    Pierwszy = new PakietDanych('T',0);
    IloscZnakow = 0;
}

void Operator::Wysw(){
if(IloscZnakow == 0){
	cout<<"\nBrak elementów tablicy\n";
}
else{
	PakietDanych * ToWysw;
	ToWysw = Pierwszy;
		for(unsigned int i = 1; i<(IloscZnakow + 1); i++){
		ToWysw = ToWysw->getNast();
		ToWysw->Wysw();
		cout<<"\n";
		}
	}
}

void Operator::UsunListe(){
	
}

PakietDanych * Operator::getElem(unsigned int NR){
if(!( 0 < NR < IloscZnakow + 1)){
		return Pierwszy;
	}
	PakietDanych * retrun = Pierwszy->getNast(); 
	for(int i = 1; i < NR ; i++){
		retrun = retrun->getNast();
	}

return retrun;
}

bool Operator::UsunElem(unsigned int NR){
	if(!(( 0 < NR)&&( NR < IloscZnakow + 1))){
		return 1;
	}
	PakietDanych * ElementUsuwany = this->getElem(NR);
	PakietDanych * Pom1 = ElementUsuwany->getPop();
	if(NR == IloscZnakow){
	Pom1->setNast(nullptr);
	}else
	{
	PakietDanych * Pom2 = ElementUsuwany->getNast();
	Pom2->setPop(Pom1);
	Pom1->setNast(Pom2);
	}
	delete ElementUsuwany;
	return 0;
}

bool Operator::UsunElem(PakietDanych * ElementUsuwany){
PakietDanych * Pom1 = ElementUsuwany->getPop();
	if(ElementUsuwany->getNast() == nullptr){
	Pom1->setNast(nullptr);
	}else
	{
	PakietDanych * Pom2 = ElementUsuwany->getNast();
	Pom2->setPop(Pom1);
	Pom1->setNast(Pom2);
	}
	delete ElementUsuwany;
	return 0;
}

void Operator::DodajElem(PakietDanych * NowyPakiet){
    PakietDanych * Pom = Pierwszy;
    PakietDanych * Pom2;
    unsigned int IloscPom = 0;
    while((NowyPakiet->getID() > Pom->getID())&&(IloscPom < IloscZnakow)){//to ustawia Pom jako
        Pom = Pom->getNast();
        IloscPom++;
    }
    if(NowyPakiet->getID() < Pom->getID()){
        NowyPakiet->setPop(Pom->getPop());
        NowyPakiet->setNast(Pom);
        Pom2 = Pom->getPop();
        Pom2->setNast(NowyPakiet);
        Pom->setPop(NowyPakiet);
    }
    else{
        NowyPakiet->setPop(Pom);
        Pom->setNast(NowyPakiet);
    }
    IloscZnakow++;
}

unsigned int Operator::getIloscZnakow(){
return IloscZnakow;
}

//                      Funkcje odpowiedzialne za interakcję z plikiem tekstowym 

void Operator::ZczytajWiadomosc(string NazwaPlikuWiadomosc){
	fstream uchwyt;
	uchwyt.open(NazwaPlikuWiadomosc);
	PakietDanych * PomPakiet;
	unsigned int PomLicznikID = 1;
	string linia,his1;
	getline(uchwyt, linia);
	while((linia != "")&&(linia!=his1)){
	 for(int i = 0; i < linia.length() ; i++){
		PomPakiet = new PakietDanych(linia[i],PomLicznikID);
		this->DodajElem(PomPakiet);
		PomLicznikID++;
		}
	 his1 = linia;
	 PomPakiet = new PakietDanych(11,PomLicznikID);
	 this->DodajElem(PomPakiet);
	 PomLicznikID++;
	 getline(uchwyt, linia);
	}
	cout<<"\n"<<"Zczytano "<<this->getIloscZnakow()<<" wedłóg operatora \n A tyle "<<PomLicznikID-1<<" wedłóg funkcji.\n";	
	uchwyt.close(); 
}

bool Operator::ZapiszJakoWiadomosc(string NazwaPlikuDoZapis){
	if(IloscZnakow < 1){return 1;}
	ofstream uchwyt;
	uchwyt.open(NazwaPlikuDoZapis);
	unsigned int LicznikZnakow = IloscZnakow;
	PakietDanych * ElementSpisujacy = Pierwszy->getNast();
	while(LicznikZnakow > 0){
		if(ElementSpisujacy->getZnak() == 11){
			uchwyt<<"\n";
		}else{
			uchwyt<<ElementSpisujacy->getZnak();
		}
		if(ElementSpisujacy->getNast() != nullptr){ElementSpisujacy = ElementSpisujacy->getNast();}
		LicznikZnakow--;
	}
	uchwyt.close();
return 0;
}

//                      Funkcje odpowiedzialne za interakcję z plikiem sygnałowym

void Operator::ZczytajSygnal(string NazwaPlikuSygnal){
	fstream uchwyt;
	int pom;
	uchwyt.open(NazwaPlikuSygnal);
	PakietDanych * PomPakiet;
	string linia ,his1;
	unsigned int pomID = 0;
	getline(uchwyt, linia);
	while((linia != "")&&(linia!=his1)){  // główna pętla
	  	for(int i = 1; i < linia.length() ; i++){//  liczenie  ID 
	  	pom = linia[i] - 48;
	 	pomID += (pom)*pow10(linia.length()-i);
	  	}
		his1 = linia;//               		  zapis historii
	  	getline(uchwyt, linia);//             pobieranie danych
		if((linia != "")||(his1[0] !=' ')){// wstawianie tabulatur w wiadomość 
	  		PomPakiet = new PakietDanych(his1[0],pomID);
		}
		else{
			PomPakiet = new PakietDanych(11,pomID);
		getline(uchwyt, linia);
		}
		  this->DodajElem(PomPakiet);
	  	pomID = 0;
	}
	uchwyt.close(); 
}



bool Operator::ZapiszJakoSygnal(string NazwaPlikuDoZapis){
	if(IloscZnakow < 1){return 1;}
	ofstream uchwyt;
	uchwyt.open(NazwaPlikuDoZapis);
	unsigned int LicznikZnakow = IloscZnakow;
	PakietDanych * ElementSpisujacy = Pierwszy;
	while(LicznikZnakow > 0){
		ElementSpisujacy = ElementSpisujacy->getNast();
		if(ElementSpisujacy->getZnak() == 11){
			uchwyt<<" ";
			uchwyt<<ElementSpisujacy->getID();
			uchwyt<<"\n";
			uchwyt<<"\n";
		}else{
			uchwyt<<ElementSpisujacy->getZnak();
			uchwyt<<ElementSpisujacy->getID();
			uchwyt<<"\n";
		}
		LicznikZnakow--;
	}
	uchwyt.close();
	return 0;
}

PakietDanych * Operator::getRandElem(){
	srand (time(NULL));
	return this->getElem(rand() % IloscZnakow + 1);
}

bool Operator::ZapiszJakoLosowySygnal(string NazwaPlikuDoZapis){
	if(IloscZnakow < 1){return 1;}
	ofstream uchwyt;
	uchwyt.open(NazwaPlikuDoZapis);
	PakietDanych * ElementSpisujacy;
	while(IloscZnakow > 0){
		ElementSpisujacy = this->getRandElem();
		if(ElementSpisujacy->getZnak() == 11){
			uchwyt<<" ";
			uchwyt<<ElementSpisujacy->getID();
			uchwyt<<"\n";
			uchwyt<<"\n";
		}else{
			uchwyt<<ElementSpisujacy->getZnak();
			uchwyt<<ElementSpisujacy->getID();
			uchwyt<<"\n";
		}
		IloscZnakow--;
		this->UsunElem(ElementSpisujacy);
	}
	uchwyt.close();
	return 0;
}