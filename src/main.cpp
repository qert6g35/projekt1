#include <string>
#include <iostream>
#include <fstream>
#include "Operator.hh"

using namespace std;

int main(int argc, char **argv){

	Operator GlowaListy;
	int Odp; 
	cout<<"\nPodaj jedną z dwóch opcji :\n"<<"  1 - z pliku text.txt > sygnalP.txt\n  2 - z pliku sygnal.txt > textP.txt\n->";
	cin>>Odp;
	cout<<"\n";
	switch(Odp){
		case 1:
			GlowaListy.ZczytajWiadomosc("text.txt");
			GlowaListy.Wysw();
			GlowaListy.ZapiszJakoLosowySygnal("sygnalP.txt");
		break;
		case 2:
			GlowaListy.ZczytajSygnal("sygnal.txt");
			GlowaListy.Wysw();
			GlowaListy.ZapiszJakoWiadomosc("textP.txt");
		break;
		default:
			cout<<"\n Podano nieprawidłową opcię programu, Proszę podać '1' lub '2' ;) \n";
			return 1;
	}

	return 0;
}
