#ifndef OPERATOR_HH
#define OPERATOR_HH

#include "Pakiet.hh"
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>     
#include <time.h>

class Operator {
    private:
        PakietDanych * Pierwszy;
        unsigned int IloscZnakow;
    public:

        Operator();//                                Operacje dot. listy i struktury Operatora
        void DodajElem(PakietDanych * NowyPakiet);// Dodaje Nowy Pakiet do listy pakietów na odpowiednie miejsce 
        bool UsunElem(unsigned int NR);//usuwa element o wskazanym miejscu w kolejce
        bool UsunElem(PakietDanych * ElementUsuwany);
        void Wysw();
        unsigned int getIloscZnakow();
        PakietDanych * getElem(unsigned int NR);//zwraca wskaźnika na element o wskazanym miejscu w kolejce
        PakietDanych * getRandElem();
        void UsunListe();

        void ZczytajSygnal(string NazwaPlikuSygnal);
        bool ZapiszJakoSygnal(string NazwaPlikuDoZapis);
        bool ZapiszJakoLosowySygnal(string NazwaPlikuDoZapis);

        void ZczytajWiadomosc(string NazwaPlikuWiadomos);
        bool ZapiszJakoWiadomosc(string NazwaPlikuDoZapis);
};

#endif
